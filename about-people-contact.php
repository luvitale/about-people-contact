<?php
/**
 * Plugin Name: About People Contact
 * Plugin URI: https://www.unesma.net
 * Description: Show profiles of related people with contact and info.
 * Version: 1.0
 * Author: Luciano Nahuel Vitale
 * Author URI: https://luvitale.net
 */

function create_people_contact_db() {
  global $wpdb;
  $people_contact_table = $wpdb->prefix . "people_contact";

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

  $created = dbDelta(
   "CREATE TABLE $people_contact_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     name varchar(100) NOT NULL DEFAULT '',
     image varchar(512) NOT NULL DEFAULT '',
     facebook varchar(100) DEFAULT NULL,
     instagram varchar(100) DEFAULT NULL,
     twitter varchar(100) DEFAULT NULL,
     linkedin varchar(100) DEFAULT NULL,
     PRIMARY KEY (id)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );
}
register_activation_hook( __FILE__, 'create_people_contact_db' );

function shortcode_show_people() {
	global $wpdb;
	$people_contact_table = $wpdb->prefix . 'people_contact';
	$people = $wpdb->get_results("SELECT * FROM $people_contact_table");
  ?>
	<div class="people-contact-grid">
	<?php foreach ($people as $person): ?>
  <?php $args = array(
     'name' => $person->name,
     'image' => $person->image,
     'facebook' => $person->facebook,
     'instagram' => $person->instagram
   );
   ?>
    <a class="person person-tile" rel="noopener">
      <img class="person-image"
        src="<?= $args['image'] ?>"
        alt="person"
      >
      <p class="person-title">
        <?= $args['name'] ?>
      </p>
    </a>
   <?php endforeach ?>
	</div><?php
}
add_shortcode('people-contact', 'shortcode_show_people');
